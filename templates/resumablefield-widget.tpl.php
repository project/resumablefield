<?php
/**
 *
 * Form widget template.
 */
?>

<div id="resumablefield--droparea">
  <?php print t('Drop files here to upload or !link', array('!link' => '<a href="#" id="resumablefield--browsebutton">' . t('select from your computer') . '</a>')); ?>
</div>
<div id="resumablefield--progressbar--wrapper">
  <div id="resumablefield--progressbar"></div>
  <a href="#" id="resumablefield--pausebutton"><?php print t('Pause'); ?></a>
  <a href="#" id="resumablefield--continuebutton"><?php print t('Continue'); ?></a>
</div>
