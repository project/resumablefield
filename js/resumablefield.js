(function ($) {

  Drupal.resumablefield = Drupal.resumablefield || {};
  Drupal.resumablefield.behaviors = Drupal.resumablefield.behaviors || {};

  /**
   * Resumablefield behaviours.
   */
  Drupal.behaviors.resumablefield = {
    attach : function(context, settings) {
      Drupal.resumablefield.attachBehaviors(context, settings);
    }
  };

  /**
   * Attach behaviors.
   */
  Drupal.resumablefield.attachBehaviors = function (context, settings) {
    $.each(Drupal.resumablefield.behaviors, function() {
      this(context, settings);
    });
  };

  /**
   * Upload file.
   */
  Drupal.resumablefield.behaviors.upload_file = function (context, settings) {
    var progressbar = $('#resumablefield--progressbar'),
      progressbar_wrapper = $('#resumablefield--progressbar--wrapper'),
      r;
    progressbar.progressbar({value: false});
    progressbar_wrapper.hide();

    r = new Resumable({
      target: '/ajax/resumablefield'
    });

    $('#resumablefield--pausebutton').click(function() {
      r.pause();
    });

    $('#resumablefield--continuebutton').click(function() {
      r.upload();
    });

    r.assignBrowse(document.getElementById('resumablefield--browsebutton'));
    r.assignDrop(document.getElementById('resumablefield--droparea'));

    r.on('fileSuccess', function(file){});
    r.on('fileProgress', function(file){
      progressbar.progressbar('option', {
        value: file.progress() * 100
      });
    });
    r.on('fileAdded', function(file, event){
      progressbar_wrapper.show();
      r.upload();
    });
    r.on('filesAdded', function(array){
      progressbar_wrapper.show();
    });
    r.on('fileRetry', function(file){});
    r.on('fileError', function(file, message){});
    r.on('uploadStart', function(){});
    r.on('complete', function(){
      progressbar_wrapper.hide();
    });
    r.on('progress', function(){});
    r.on('error', function(message, file){});
    r.on('pause', function(){});
    r.on('cancel', function(){});
  };

})(jQuery);
