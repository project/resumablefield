<?php
/**
 * @file
 * Provides the basic object definitions used by resumablefield.
 */

/**
 * ResumableFieldController.
 */
class ResumableFieldController {
  /**
   * Default constructor.
   */
  public function __construct() {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {

      $temp_dir = 'temp/' . $_GET['resumableIdentifier'];
      $chunk_file = $temp_dir . '/' . $_GET['resumableFilename'] . '.part' . $_GET['resumableChunkNumber'];
      if (file_exists($chunk_file)) {
        header("HTTP/1.0 200 Ok");
      }
      else {
        header("HTTP/1.0 404 Not Found");
      }
    }

    // Loop through files and move the chunks to a temporarily created directory.
    if (!empty($_FILES)) {
      foreach ($_FILES as $file) {
        // Check the error status.
        if ($file['error'] != 0) {
          $this->log('error ' . $file['error'] . ' in file ' . $_POST['resumableFilename']);
          continue;
        }

        // Init the destination file (format <filename.ext>.part<#chunk>
        // the file is stored in a temporary directory.
        $temp_dir = 'temp/' . $_POST['resumableIdentifier'];
        $dest_file = $temp_dir . '/' . $_POST['resumableFilename'] . '.part' . $_POST['resumableChunkNumber'];

        // Create the temporary directory.
        if (!is_dir($temp_dir)) {
          mkdir($temp_dir, 0777, TRUE);
        }

        // Move the temporary file.
        if (!move_uploaded_file($file['tmp_name'], $dest_file)) {
          $this->log('Error saving (move_uploaded_file) chunk ' . $_POST['resumableChunkNumber'] . ' for file ' . $_POST['resumableFilename']);
        }
        else {
          // Check if all the parts present, and create the final destination file.
          $this->createFileFromChunks($temp_dir, $_POST['resumableFilename'],
            $_POST['resumableChunkSize'], $_POST['resumableTotalSize']);
        }
      }
    }
  }

  /**
   * Check if all the parts exist, and gather all the parts of the file together.
   *
   * @param string $temp_dir
   *   The temporary directory holding all the parts of the file.
   *
   * @param string $file_name
   *   The original file name.
   *
   * @param string $chunk_size
   *   Each chunk size (in bytes).
   *
   * @param string $total_size
   *   Original file size (in bytes).
   *
   * @return bool
   *   Success or false.
   */
  public function createFileFromChunks($temp_dir, $file_name, $chunk_size, $total_size) {
    // Count all the parts of this file.
    $total_files = 0;
    foreach (scandir($temp_dir) as $file) {
      if (stripos($file, $file_name) !== FALSE) {
        $total_files++;
      }
    }

    // Check that all the parts are present.
    // The size of the last part is between $chunk_size and 2 * $chunk_size.
    if ($total_files * $chunk_size >= ($total_size - $chunk_size + 1)) {
      // Create the final destination file.
      if (($fp = fopen('temp/' . $file_name, 'w')) !== FALSE) {
        for ($i = 1; $i <= $total_files; $i++) {
          fwrite($fp, file_get_contents($temp_dir . '/' . $file_name . '.part' . $i));
          $this->log('writing chunk ' . $i);
        }
        fclose($fp);
      }
      else {
        $this->log('cannot create the destination file');
        return TRUE;
      }

      // Rename the temporary directory (to avoid access from other
      // concurrent chunks uploads) and than delete it.
      if (rename($temp_dir, $temp_dir . '_UNUSED')) {
        $this->rrmdir($temp_dir . '_UNUSED');
      }
      else {
        $this->rrmdir($temp_dir);
      }
    }
    return TRUE;
  }

  /**
   * Delete a directory RECURSIVELY.
   *
   * @param string $dir
   *   Directory path.
   *
   * @link http://php.net/manual/en/function.rmdir.php
   */
  public function rrmdir($dir) {
    if (is_dir($dir)) {
      $objects = scandir($dir);
      foreach ($objects as $object) {
        if ($object != "." && $object != "..") {
          if (filetype($dir . "/" . $object) == "dir") {
            $this->rrmdir($dir . "/" . $object);
          }
          else {
            unlink($dir . "/" . $object);
          }
        }
      }
      reset($objects);
      rmdir($dir);
    }
  }

  /**
   * Logging operation - to a file (upload_log.txt) and to the stdout.
   *
   * @param string $str
   *   The logging string.
   */
  public function log($str) {
    // Log to the output.
    $log_str = date('d.m.Y') . ": {$str}\r\n";
    echo $log_str;

    // Log to file.
    if (($fp = fopen('upload_log.txt', 'a+')) !== FALSE) {
      fwrite($fp, $log_str);
      fclose($fp);
    }
  }
}
