<?php
/**
 * @file
 * Provides field type which can handle multiple simultaneous, stable and
 * resumable uploads via the HTML5 File API.
 */

/**
 * Implements hook_menu().
 */
function resumablefield_menu() {
  $items = array();

  $items['ajax/resumablefield'] = array(
    'title' => 'Title',
    'description' => 'Description',
    'access callback' => TRUE,
    'page callback' => 'resumablefield_upload_callback',
    'page arguments' => array(),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Upload callback.
 */
function resumablefield_upload_callback() {
  $handler = new ResumableFieldController();
}

/**
 * Implements hook_libraries_info().
 */
function resumablefield_libraries_info() {
  $libraries['resumable.js'] = array(
    'name' => 'Resumable JS',
    'vendor url' => 'https://github.com/23/resumable.js',
    'download url' => 'https://github.com/23/resumable.js/archive/master.zip',
    'version' => '1.0',
    'files' => array(
      'js' => array(
        'resumable.js',
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_field_widget_info().
 */
function resumablefield_field_widget_info() {
  return array(
    'resumablefield' => array(
      'label' => t('Resumable Field'),
      'field types' => array('image', 'file'),
      'settings' => array(),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function resumablefield_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $library = libraries_detect('resumable.js');

  $element['#attached']['library'][] = array('system', 'ui.progressbar');
  $element['#attached']['js'][] = $library['library path'] . '/resumable.js';
  $element['#attached']['js'][] = drupal_get_path('module', 'resumablefield') . '/js/resumablefield.js';
  $element['#attached']['css'][] = drupal_get_path('module', 'resumablefield') . '/css/resumablefield.css';
  $element['#markup'] = theme('resumablefield_widget');

  return $element;
}

/**
 * Implements hook_theme().
 */
function resumablefield_theme() {
  return array(
    'resumablefield_widget' => array(
      'variables' => array(),
      'template' => 'resumablefield-widget',
      'path' => drupal_get_path('module', 'resumablefield') . '/templates',
    ),
  );
}
